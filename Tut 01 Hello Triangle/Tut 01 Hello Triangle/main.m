//
//  main.m
//  Tut 01 Hello Triangle
//
//  Created by Eric Berna on 12/1/11.
//  Copyright (c) 2011 CI Design Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Tut01AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([Tut01AppDelegate class]));
	}
}

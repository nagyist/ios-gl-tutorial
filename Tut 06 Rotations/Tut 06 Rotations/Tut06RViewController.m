//
//  Tut06RViewController.m
//  Tut 06 Rotations
//
//  Copyright (c) 2012 Eric Berna.
//
//  Based on code by Jason McKesson.
//  Copyright (C) 2010-2012 by Jason L. McKesson
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notices and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "Tut06RViewController.h"
#import "Tut06RInstance.h"

#define ARRAY_COUNT( array ) (sizeof( array ) / (sizeof( array[0] ) * (sizeof( array ) != sizeof(void*) || sizeof( array[0] ) <= sizeof(void*))))


#define GREEN_COLOR 0.0f, 1.0f, 0.0f, 1.0f
#define BLUE_COLOR 	0.0f, 0.0f, 1.0f, 1.0f
#define RED_COLOR 1.0f, 0.0f, 0.0f, 1.0f
#define GREY_COLOR 0.8f, 0.8f, 0.8f, 1.0f
#define BROWN_COLOR 0.5f, 0.5f, 0.0f, 1.0f

const int numberOfVertices = 8;

const GLfloat vertexData[] =
{
	+1.0f, +1.0f, +1.0f,
	-1.0f, -1.0f, +1.0f,
	-1.0f, +1.0f, -1.0f,
	+1.0f, -1.0f, -1.0f,
	
	-1.0f, -1.0f, -1.0f,
	+1.0f, +1.0f, -1.0f,
	+1.0f, -1.0f, +1.0f,
	-1.0f, +1.0f, +1.0f,
	
	GREEN_COLOR,
	BLUE_COLOR,
	RED_COLOR,
	BROWN_COLOR,
	
	GREEN_COLOR,
	BLUE_COLOR,
	RED_COLOR,
	BROWN_COLOR,
};

const GLshort indexData[] =
{
	0, 1, 2,
	1, 0, 3,
	2, 3, 0,
	3, 2, 1,
	
	5, 4, 6,
	4, 5, 7,
	7, 6, 4,
	6, 7, 5,
};




@interface Tut06RViewController () {
	GLuint _program;
    GLuint vertexBufferObject;
	GLuint indexBufferObject;
	GLuint vao;
	GLuint modelToCameraMatrixUnif;
	GLuint cameraToClipMatrixUnif;
	GLKMatrix4 cameraToClipMatrix;
	float fFrustumScale;
}

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) NSArray *instances;

- (void)setupGL;
- (void)tearDownGL;

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;
@end

@implementation Tut06RViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
	
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
	Tut06RInstance *NullRotation = [[Tut06RNullRotationInstance alloc] initWithOffset:GLKVector4Make(0.0f, 0.0f, -25.0f, 1.0f)];
	Tut06RInstance *RotateX = [[Tut06RRotateXInstance alloc] initWithOffset:GLKVector4Make(-5.0f, -5.0f, -25.0f, 1.0f)];
	Tut06RInstance *RotateY = [[Tut06RRotateYInstance alloc] initWithOffset:GLKVector4Make(-5.0f, 5.0f, -25.0f, 1.0f)];
	Tut06RInstance *RotateZ = [[Tut06RRotateZInstance alloc] initWithOffset:GLKVector4Make(5.0f, 5.0f, -25.0f, 1.0f)];
	Tut06RInstance *RotateAxis = [[Tut06RRotateAxisInstance alloc] initWithOffset:GLKVector4Make(5.0f, -5.0f, -25.0f, 1.0f)];
	self.instances = @[NullRotation, RotateX, RotateY, RotateZ, RotateAxis];
	
	fFrustumScale = [self calcFrustumScale:45.0f];
	[self setupGL];
	
}

- (void)dealloc
{
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
	
    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }
	
		// Dispose of any resources that can be recreated.
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    
    [self loadShaders];
	
	modelToCameraMatrixUnif = glGetUniformLocation(_program, "modelToCameraMatrix");
	cameraToClipMatrixUnif = glGetUniformLocation(_program, "cameraToClipMatrix");
	
	
	float fzNear = 1.0f; float fzFar = 45.0f;
	cameraToClipMatrix = GLKMatrix4Identity;
	cameraToClipMatrix.m00 = fFrustumScale;
	cameraToClipMatrix.m11 = fFrustumScale;
	cameraToClipMatrix.m22 = (fzFar + fzNear) / (fzNear - fzFar);
	cameraToClipMatrix.m23 = -1.0f;
	cameraToClipMatrix.m32 = (2 * fzFar * fzNear) / (fzNear - fzFar);
	cameraToClipMatrix.m33 = 0.0f;
	glUseProgram(_program);
	glUniformMatrix4fv(cameraToClipMatrixUnif, 1, GL_FALSE, cameraToClipMatrix.m);
	glUseProgram(0);
	
	
	[self initializeVertexBuffer];
    
    
    glGenVertexArraysOES(1, &vao);
    glBindVertexArrayOES(vao);
	
	size_t colorDataOffset = sizeof(float) * 3 * numberOfVertices;
	
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)colorDataOffset);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	
	glBindVertexArrayOES(0);
	
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CW);
	
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDepthRangef(0.0f, 1.0f);
}

- (GLfloat)calcFrustumScale:(GLfloat) fFovDeg {
	const GLfloat degToRad = M_PI * 2.0f / 360.0f;
	GLfloat fFovRad = fFovDeg * degToRad;
	return 1.0f / tanf(fFovRad / 2.0f);
}


- (void) initializeVertexBuffer
{
	glGenBuffers(1, &vertexBufferObject);
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glGenBuffers(1, &indexBufferObject);
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexData), indexData, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    glDeleteBuffers(1, &vertexBufferObject);
    glDeleteVertexArraysOES(1, &vao);
    
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}

#pragma mark - GLKView and GLKViewController delegate methods


- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepthf(1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(_program);
	
	glBindVertexArrayOES(vao);
	
	for (Tut06RInstance *instance in self.instances) {
		GLfloat elapsedTime = (GLfloat)self.timeSinceFirstResume;
		GLKMatrix4 transformMatrix = [instance constructMatrixWithElapsedTime:elapsedTime];
		glUniformMatrix4fv(modelToCameraMatrixUnif, 1, GL_FALSE, transformMatrix.m);
		glDrawElements(GL_TRIANGLES, ARRAY_COUNT(indexData), GL_UNSIGNED_SHORT, 0);
	}
	
	glBindVertexArrayOES(0);
	glUseProgram(0);
	
}

#pragma mark - Reshape for iOS

	// GLUT's reshape function is called whenever the window is resized.
	// On iOS views generally are only resized on rotation events. Here
	// the adjustment code is a separate method, so it can be called by
	// the event handling methods Cocoa expects to be overridden by a
	// subclass of UIViewController.
	//
	// A more advanced game could have a GLKView that changes size
	// depending upon progression through the game. The
	// adjustAspectRatio method could be called by the game logic in
	// such apps.

- (void)adjutAspectRatio {
	GLKView *glkView = (GLKView *)self.view;
		// At early stages of setup for the GLKView drawableHeight can be zero.
	if (glkView.drawableHeight != 0) {
		cameraToClipMatrix.m00 = fFrustumScale * ((GLfloat)glkView.drawableHeight / (GLfloat)glkView.drawableWidth);
	}
	cameraToClipMatrix.m11 = fFrustumScale;
	glUseProgram(_program);
	glUniformMatrix4fv(cameraToClipMatrixUnif, 1, GL_FALSE, cameraToClipMatrix.m );
	glUseProgram(0);
	glViewport(0, 0, glkView.drawableWidth, glkView.drawableHeight);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
	[self adjutAspectRatio];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[self adjutAspectRatio];
}


#pragma mark -  OpenGL ES 2 shader compilation

- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
		// Create shader program.
    _program = glCreateProgram();
    
		// Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"PosColorLocalTransform" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
		// Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"ColorPassthrough" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
		// Attach vertex shader to program.
    glAttachShader(_program, vertShader);
    
		// Attach fragment shader to program.
    glAttachShader(_program, fragShader);
    
		// Bind attribute locations.
		// This needs to be done prior to linking.
    glBindAttribLocation(_program, GLKVertexAttribPosition, "position");
	glBindAttribLocation(_program, 1, "color");
    
		// Link program.
    if (![self linkProgram:_program]) {
        NSLog(@"Failed to link program: %d", _program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_program) {
            glDeleteProgram(_program);
            _program = 0;
        }
        
        return NO;
    }
    
    
		// Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(_program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_program, fragShader);
        glDeleteShader(fragShader);
    }
	
	
	glUseProgram(_program);
	glUseProgram(0);
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

@end

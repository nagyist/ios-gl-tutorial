//
//  Tut06RInstance.h
//  Tut 06 Rotations
//
//  Copyright (c) 2012 Eric Berna.
//
//  Based on code by Jason McKesson.
//  Copyright (C) 2010-2012 by Jason L. McKesson
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notices and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@interface Tut06RInstance : NSObject

- (id)initWithOffset:(GLKVector4)offset;
- (GLKMatrix4)calcRotationWithElapsedTime:(GLfloat)elapsedTime;
- (GLKMatrix4)constructMatrixWithElapsedTime:(GLfloat)elapsedTime;

@end

@interface Tut06RNullRotationInstance : Tut06RInstance

@end

@interface Tut06RRotateXInstance : Tut06RInstance

@end

@interface Tut06RRotateYInstance : Tut06RInstance

@end

@interface Tut06RRotateZInstance : Tut06RInstance

@end

@interface Tut06RRotateAxisInstance : Tut06RInstance

@end

//
//  Tut06RInstance.m
//  Tut 06 Rotations
//
//  Copyright (c) 2012 Eric Berna.
//
//  Based on code by Jason McKesson.
//  Copyright (C) 2010-2012 by Jason L. McKesson
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notices and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "Tut06RInstance.h"
#import <GLKit/GLKit.h>

@interface Tut06RInstance ()

@property (nonatomic) GLKVector4 offset;

- (GLfloat)calcLerpFactorWithElapsedTime:(GLfloat)elapsedTime loopDuration:(GLfloat)loopDuration;
- (GLfloat)computeAngleRadWithElapsedTime:(GLfloat)elapsedTime loopDuration:(GLfloat)loopDuration;
@end

@implementation Tut06RInstance

- (id)initWithOffset:(GLKVector4)offset
{
    self = [super init];
    if (self) {
        _offset = offset;
    }
    return self;
}
- (GLKMatrix4)calcRotationWithElapsedTime:(GLfloat)elapsedTime {
	return GLKMatrix4Identity;
}
- (GLKMatrix4)constructMatrixWithElapsedTime:(GLfloat)elapsedTime {
	GLKMatrix4 theMat = [self calcRotationWithElapsedTime:elapsedTime];
	theMat = GLKMatrix4SetColumn(theMat, 3, self.offset);
	return theMat;
}

- (GLfloat)calcLerpFactorWithElapsedTime:(GLfloat)elapsedTime loopDuration:(GLfloat)loopDuration {
	GLfloat fValue = fmodf(elapsedTime, loopDuration) / loopDuration;
	if (fValue > 0.5f) {
		fValue = 1.0f - fValue;
	}
	return fValue * 2.0f;
}

- (GLfloat)computeAngleRadWithElapsedTime:(GLfloat)elapsedTime loopDuration:(GLfloat)loopDuration {
	const GLfloat fScale = M_PI * 2.0f / loopDuration;
	GLfloat fCurrTimeThroughLoop = fmodf(elapsedTime, loopDuration);
	return fCurrTimeThroughLoop * fScale;
}

@end

@implementation Tut06RNullRotationInstance


@end

@implementation Tut06RRotateXInstance

- (GLKMatrix4)calcRotationWithElapsedTime:(GLfloat)elapsedTime {
	GLfloat fAngRad = [self computeAngleRadWithElapsedTime:elapsedTime loopDuration:3.0f];
	GLfloat fCos = cosf(fAngRad);
	GLfloat fSin = sinf(fAngRad);
	GLKMatrix4 theMat = GLKMatrix4Identity;
	theMat.m11 = fCos;
	theMat.m21 = -fSin;
	theMat.m12 = fSin;
	theMat.m22 = fCos;
	return theMat;	
}

@end

@implementation Tut06RRotateYInstance

- (GLKMatrix4)calcRotationWithElapsedTime:(GLfloat)elapsedTime {
	GLfloat fAngRad = [self computeAngleRadWithElapsedTime:elapsedTime loopDuration:2.0f];
	GLfloat fCos = cosf(fAngRad);
	GLfloat fSin = sinf(fAngRad);
	GLKMatrix4 theMat = GLKMatrix4Identity;
	theMat.m00 = fCos;
	theMat.m20 = fSin;
	theMat.m02 = -fSin;
	theMat.m22 = fCos;
	return theMat;
}

@end

@implementation Tut06RRotateZInstance

- (GLKMatrix4)calcRotationWithElapsedTime:(GLfloat)elapsedTime {
	GLfloat fAngRad = [self computeAngleRadWithElapsedTime:elapsedTime loopDuration:2.0f];
	GLfloat fCos = cosf(fAngRad);
	GLfloat fSin = sinf(fAngRad);
	GLKMatrix4 theMat = GLKMatrix4Identity;
	theMat.m00 = fCos;
	theMat.m10 = -fSin;
	theMat.m01 = fSin;
	theMat.m11 = fCos;
	return theMat;
}


@end

@implementation Tut06RRotateAxisInstance

- (GLKMatrix4)calcRotationWithElapsedTime:(GLfloat)elapsedTime {
	GLfloat fAngRad = [self computeAngleRadWithElapsedTime:elapsedTime loopDuration:2.0f];
	GLfloat fCos = cosf(fAngRad);
	GLfloat fInvCos = 1.0f - fCos;
	GLfloat fSin = sinf(fAngRad);
		//GLfloat	fInvSin = 1.0f - fSin;
	
	GLKVector4 axis = GLKVector4Make(1.0f, 1.0f, 1.0f, 1.0f);
	GLKMatrix4 theMat = GLKMatrix4Identity;
	
	theMat.m00 = (axis.x * axis.x) + ((1 - axis.x * axis.x) * fCos);
	theMat.m10 = axis.x * axis.y * (fInvCos) - (axis.z * fSin);
	theMat.m20 = axis.x * axis.z * (fInvCos) + (axis.y * fSin);
	
	theMat.m01 = axis.x * axis.y * (fInvCos) + (axis.z * fSin);
	theMat.m11 = (axis.y * axis.y) + ((1 - axis.y * axis.y) * fCos);
	theMat.m21 = axis.y * axis.z * (fInvCos) - (axis.x * fSin);
	
	theMat.m02 = axis.x * axis.z * (fInvCos) - (axis.y * fSin);
	theMat.m12 = axis.y * axis.z * (fInvCos) + (axis.x * fSin);
	theMat.m22 = (axis.z * axis.z) + ((1 - axis.z * axis.z) * fCos);
	
	return theMat;
}


@end





//
//  Tut06HHierarchy.m
//  Tut 06 Hierarchy
//
//  Copyright (c) 2012 Eric Berna.
//
//  Based on code by Jason McKesson.
//  Copyright (C) 2010-2012 by Jason L. McKesson
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notices and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "Tut06HHierarchy.h"

enum Tut06HierarchyParts {
	kLeftBase = 0,
	kRightBase,
	kLowerArm,
	kUpperArm,
	kWrist,
	kLeftFinger,
	kLeftLowerFinger,
	kRightFinger,
	kRightLowerFinger,
	kPartsCount
};

enum Tut06HierarchyControls {
	kBaseSpin = 0,
	kArmRaise,
	kElbowRaise,
	kWristRaise,
	kWristSpin,
	kFingerOpen,
	kControlsCount
};

GLfloat Clamp(GLfloat fValue, GLfloat fMinValue, GLfloat fMaxValue)
{
	if(fValue < fMinValue)
		return fMinValue;
	
	if(fValue > fMaxValue)
		return fMaxValue;
	
	
	return fValue;
}

GLfloat DegToRad(GLfloat fAngDeg)
{
	const GLfloat fDegToRad = M_PI * 2.0f / 360.0f;
	return fAngDeg * fDegToRad;
}


@interface Tut06HHierarchy ()

@property (nonatomic) GLKVector3 posBase;
@property (nonatomic) GLfloat angBase;
@property (nonatomic) GLfloat tempAngBase;
@property (nonatomic) GLKVector3 posBaseLeft;
@property (nonatomic) GLKVector3 posBaseRight;
@property (nonatomic) GLfloat scaleBaseZ;
@property (nonatomic) GLfloat angUpperArm;
@property (nonatomic) GLfloat tempAngUpperArm;
@property (nonatomic) GLfloat sizeUpperArm;
@property (nonatomic) GLKVector3 posLowerArm;
@property (nonatomic) GLfloat angLowerArm;
@property (nonatomic) GLfloat tempAngLowerArm;
@property (nonatomic) GLfloat lenLowerArm;
@property (nonatomic) GLfloat widthLowerArm;
@property (nonatomic) GLKVector3 posWrist;
@property (nonatomic) GLfloat angWristRoll;
@property (nonatomic) GLfloat tempAngWristRoll;
@property (nonatomic) GLfloat angWristPitch;
@property (nonatomic) GLfloat tempAngWristPitch;
@property (nonatomic) GLfloat lenWrist;
@property (nonatomic) GLfloat widthWrist;
@property (nonatomic) GLKVector3 posLeftFinger;
@property (nonatomic) GLKVector3 posRightFinger;
@property (nonatomic) GLfloat angFingerOpen;
@property (nonatomic) GLfloat tempAngFingerOpen;
@property (nonatomic) GLfloat lenFinger;
@property (nonatomic) GLfloat widthFinger;
@property (nonatomic) GLfloat angLowerFinger;

@property (nonatomic) GLKMatrix4 leftBaseMatrix;
@property (nonatomic) GLKMatrix4 rightBaseMatrix;
@property (nonatomic) GLKMatrix4 lowerArmMatrix;
@property (nonatomic) GLKMatrix4 upperArmMatrix;
@property (nonatomic) GLKMatrix4 wristMatrix;
@property (nonatomic) GLKMatrix4 leftFingerMatrix;
@property (nonatomic) GLKMatrix4 leftLowerFingerMatrix;
@property (nonatomic) GLKMatrix4 rightFingerMatrix;
@property (nonatomic) GLKMatrix4 rightLowerFingerMatrix;

@end

@implementation Tut06HHierarchy

- (id)init
{
    self = [super init];
    if (self) {
			// The original configuration places the model too low. Moving the model up along the Y axis clears the controls. 
		_posBase = GLKVector3Make(3.0f, 0.0f, -40.0f);
		_angBase = -45.0f;
		_tempAngBase = _angBase;
		_posBaseLeft = GLKVector3Make(2.0f, 0.0f, 0.0f);
		_posBaseRight = GLKVector3Make(-2.0f, 0.0f, 0.0f);
		_scaleBaseZ = 3.0f;
		_angUpperArm = -33.75f;
		_tempAngUpperArm = _angUpperArm;
		_sizeUpperArm = 9.0f;
		_posLowerArm = GLKVector3Make(0.0f, 0.0f, 8.0f);
		_angLowerArm = 146.25f;
		_tempAngLowerArm = _angLowerArm;
		_lenLowerArm = 5.0f;
		_widthLowerArm = 1.5f;
		_posWrist = GLKVector3Make(0.0f, 0.0f, 5.0f);
		_angWristRoll = 0.0f;
		_tempAngWristRoll = _angWristRoll;
		_angWristPitch = 67.5f;
		_tempAngWristPitch = _angWristPitch;
		_lenWrist = 2.0f;
		_widthWrist = 2.0f;
		_posLeftFinger = GLKVector3Make(1.0f, 0.0f, 1.0f);
		_posRightFinger = GLKVector3Make(-1.0f, 0.0f, 1.0f);
		_angFingerOpen = 180.0f;
		_tempAngFingerOpen = _angFingerOpen;
		_lenFinger = 2.0f;
		_widthFinger = 0.5f;
		_angLowerFinger = 45.0f;
		[self updateMatrixes];
    }
    return self;
}

- (NSUInteger)partsCount {
	return kPartsCount;
}

- (GLKMatrix4)modelToCameraMatrixForItemAtIndex:(NSUInteger)index {
	GLKMatrix4 result = GLKMatrix4Identity;
	switch (index) {
		case kLeftBase:
			result = self.leftBaseMatrix;
			break;
		case kRightBase:
			result = self.rightBaseMatrix;
			break;
		case kLowerArm:
			result = self.lowerArmMatrix;
			break;
		case kUpperArm:
			result = self.upperArmMatrix;
			break;
		case kWrist:
			result = self.wristMatrix;
			break;
		case kLeftFinger:
			result = self.leftFingerMatrix;
			break;
		case kLeftLowerFinger:
			result = self.leftLowerFingerMatrix;
			break;
		case kRightFinger:
			result = self.rightFingerMatrix;
			break;
		case kRightLowerFinger:
			result = self.rightLowerFingerMatrix;
			break;
		default:
			break;
	}
	return result;
}

- (void)lockValuesAtControlIndex:(NSUInteger)index {
	switch (index) {
		case kBaseSpin:
			self.angBase = self.tempAngBase;
			break;
		case kArmRaise:
			self.angUpperArm = self.tempAngUpperArm;
			break;
		case kElbowRaise:
			self.angLowerArm = self.tempAngLowerArm;
			break;
		case kWristRaise:
			self.angWristPitch = self.tempAngWristPitch;
			break;
		case kWristSpin:
			self.angWristRoll = self.tempAngWristRoll;
			break;
		case kFingerOpen:
			self.angFingerOpen = self.tempAngFingerOpen;
			break;			
		default:
			break;
	}
}

- (void)updateHierarchyWithControlIndex:(NSUInteger)index delta:(GLfloat)delta {
	switch (index) {
		case kBaseSpin:
				// Adjust base angle.
			self.tempAngBase = self.angBase + (180.0f * delta);
			break;
		case kArmRaise:
				// Adjust upper arm angle.
			self.tempAngUpperArm = self.angUpperArm + (45.0f * delta);
			self.tempAngUpperArm = Clamp(self.tempAngUpperArm, -90.0f, 0.0f);
			break;
		case kElbowRaise:
				// Adjust lower arm
			self.tempAngLowerArm = self.angLowerArm + ((146.25f / 2.0f) * delta);
			self.tempAngLowerArm = Clamp(self.tempAngLowerArm, 0.0f, 146.25f);
			break;
		case kWristRaise:
				// Adjust wrist pitch.
			self.tempAngWristPitch = self.angWristPitch + (45.0f * delta);
			self.tempAngWristPitch = Clamp(self.tempAngWristPitch, 0.0f, 90.0f);
			break;
		case kWristSpin:
				// Adjust wrist roll.
			self.tempAngWristRoll = self.angWristRoll + (180.0f * delta);
			break;
		case kFingerOpen:
				// Adjust finger open angle.
			self.tempAngFingerOpen = self.angFingerOpen + ((171.0f / 2.0f) * delta);
			self.tempAngFingerOpen = Clamp(self.tempAngFingerOpen, 9.0f, 180.0f);
			break;
		default:
			break;
	}
	[self updateMatrixes];
}

- (void)updateMatrixes {
	GLKMatrixStackRef modelToCameraStack = GLKMatrixStackCreate(CFAllocatorGetDefault());
	
	GLKMatrixStackTranslateWithVector3(modelToCameraStack, self.posBase);
	GLKMatrixStackRotateY(modelToCameraStack, DegToRad(self.tempAngBase));
		// Left base matrix
	
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslateWithVector3(modelToCameraStack, self.posBaseLeft);
	GLKMatrixStackScale(modelToCameraStack, 1.0f, 1.0f, self.scaleBaseZ);
	self.leftBaseMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	
		// Right base matrix
	
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslateWithVector3(modelToCameraStack, self.posBaseRight);
	GLKMatrixStackScale(modelToCameraStack, 1.0f, 1.0f, self.scaleBaseZ);
	self.rightBaseMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	
		// Upper arm matrix
	
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackRotateX(modelToCameraStack, DegToRad(self.tempAngUpperArm));
	
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslate(modelToCameraStack, 0.0f, 0.f, (self.sizeUpperArm / 2.0f) - 1.0f);
	GLKMatrixStackScale(modelToCameraStack, 1.0f, 1.0f, self.sizeUpperArm / 2.0);
	self.upperArmMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	
		// Lower arm matrix
	
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslateWithVector3(modelToCameraStack, self.posLowerArm);
	GLKMatrixStackRotateX(modelToCameraStack, DegToRad(self.tempAngLowerArm));
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslate(modelToCameraStack, 0.0f, 0.0f, self.lenLowerArm / 2.0f);
	GLKMatrixStackScale(modelToCameraStack, self.widthLowerArm / 2.0, self.widthLowerArm / 2.0, self.lenLowerArm / 2.0f);
	self.lowerArmMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
		// Wrist matrix
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslateWithVector3(modelToCameraStack, self.posWrist);
	GLKMatrixStackRotateZ(modelToCameraStack, DegToRad(self.tempAngWristRoll));
	GLKMatrixStackRotateX(modelToCameraStack, DegToRad(self.tempAngWristPitch));
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackScale(modelToCameraStack, self.widthWrist / 2.0f, self.widthWrist / 2.0, self.lenWrist / 2.0f);
	self.wristMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
		// Left finger matrix
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslateWithVector3(modelToCameraStack, self.posLeftFinger);
	GLKMatrixStackRotateY(modelToCameraStack, DegToRad(self.tempAngFingerOpen));
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslate(modelToCameraStack, 0.0f, 0.0f, self.lenFinger / 2.0f);
	GLKMatrixStackScale(modelToCameraStack, self.widthFinger / 2.0f, self.widthFinger/ 2.0f, self.lenFinger / 2.0f);
	self.leftFingerMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
		// Left lower finger matrix
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslate(modelToCameraStack, 0.0f, 0.0f, self.lenFinger);
	GLKMatrixStackRotateY(modelToCameraStack, DegToRad(-self.angLowerFinger));
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslate(modelToCameraStack, 0.0f, 0.0f, self.lenFinger / 2.0f);
	GLKMatrixStackScale(modelToCameraStack, self.widthFinger / 2.0f, self.widthFinger/ 2.0f, self.lenFinger / 2.0f);
	self.leftLowerFingerMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
		// Right finger matrix
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslateWithVector3(modelToCameraStack, self.posRightFinger);
	GLKMatrixStackRotateY(modelToCameraStack, DegToRad(-self.tempAngFingerOpen));
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslate(modelToCameraStack, 0.0f, 0.0f, self.lenFinger / 2.0f);
	GLKMatrixStackScale(modelToCameraStack, self.widthFinger / 2.0f, self.widthFinger/ 2.0f, self.lenFinger / 2.0f);
	self.rightFingerMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
		// Right lower finger matrix
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslate(modelToCameraStack, 0.0f, 0.0f, self.lenFinger);
	GLKMatrixStackRotateY(modelToCameraStack, DegToRad(self.angLowerFinger));
	GLKMatrixStackPush(modelToCameraStack);
	GLKMatrixStackTranslate(modelToCameraStack, 0.0f, 0.0f, self.lenFinger / 2.0f);
	GLKMatrixStackScale(modelToCameraStack, self.widthFinger / 2.0f, self.widthFinger/ 2.0f, self.lenFinger / 2.0f);
	self.rightLowerFingerMatrix = GLKMatrixStackGetMatrix4(modelToCameraStack);
	
	GLKMatrixStackPop(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	GLKMatrixStackPop(modelToCameraStack);
	
	CFRelease(modelToCameraStack);
}


@end

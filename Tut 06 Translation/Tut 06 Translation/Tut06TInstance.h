//
//  Tut06TInstance.h
//  Tut 06 Translation
//
//  Created by Eric Berna on 12/4/12.
//  Copyright (c) 2012 Eric Berna. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@interface Tut06TInstance : NSObject

- (GLKVector4)calcOffset:(GLfloat)fElapsedTime;
- (GLKMatrix4)constructMatrix:(GLfloat)fElapsedTime;

@end

@interface Tut06TStationaryInstance : Tut06TInstance

@end

@interface Tut06TOvalInstance : Tut06TInstance

@end

@interface Tut06TBottomCircleInstance : Tut06TInstance

@end
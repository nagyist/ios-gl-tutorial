//
//  Tut06TInstance.m
//  Tut 06 Translation
//
//  Created by Eric Berna on 12/4/12.
//  Copyright (c) 2012 Eric Berna. All rights reserved.
//

#import "Tut06TInstance.h"

@implementation Tut06TInstance

- (GLKVector4)calcOffset:(GLfloat)fElapsedTime {
	return GLKVector4Make(1.0f, 1.0f, 1.0f, 1.0f);
}

- (GLKMatrix4)constructMatrix:(GLfloat)fElapsedTime {
	GLKMatrix4 result = GLKMatrix4Identity;
	result = GLKMatrix4SetColumn(result, 3, [self calcOffset:fElapsedTime]);
	return result;
}


@end

@implementation Tut06TStationaryInstance

- (GLKVector4)calcOffset:(GLfloat)fElapsedTime {

	return GLKVector4Make(0.0f, 0.0f, -20.0f, 1.0f);
}

@end

@implementation Tut06TOvalInstance

- (GLKVector4)calcOffset:(GLfloat)fElapsedTime {
	const GLfloat fLoopDuration = 3.0f;
	const GLfloat fScale = M_PI * 2.0f / fLoopDuration;
	GLfloat fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);
	GLfloat x = cosf(fCurrTimeThroughLoop * fScale) * 4.0f;
	GLfloat y = sinf(fCurrTimeThroughLoop * fScale) * 6.0f;
	return GLKVector4Make(x, y, -20.0f, 1.0f);
}

@end

@implementation Tut06TBottomCircleInstance

- (GLKVector4)calcOffset:(GLfloat)fElapsedTime {
	const GLfloat fLoopDuration = 3.0f;
	const GLfloat fScale = M_PI * 2.0f / fLoopDuration;
	GLfloat fCurrTimeThroughLoop = fmodf(fElapsedTime, fLoopDuration);
	GLfloat x = cosf(fCurrTimeThroughLoop * fScale) * 5.0f;
	GLfloat z = sinf(fCurrTimeThroughLoop * fScale) * 5.0f - 20.0f;
	return GLKVector4Make(x, -3.5f, z, 1.0f);
}


@end


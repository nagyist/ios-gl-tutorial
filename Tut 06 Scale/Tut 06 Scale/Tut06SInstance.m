//
//  Tut06SInstance.m
//  Tut 06 Scale
//
//  Copyright (c) 2012 Eric Berna.
//
//  Based on code by Jason McKesson.
//  Copyright (C) 2010-2012 by Jason L. McKesson
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notices and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "Tut06SInstance.h"
#import <GLKit/GLKit.h>

@interface Tut06SInstance ()

@property (nonatomic) GLKVector4 offset;

- (GLfloat)calcLerpFactorWithElapsedTime:(GLfloat)elapsedTime loopDuration:(GLfloat)loopDuration;

@end

@implementation Tut06SInstance

- (id)initWithOffset:(GLKVector4)offset
{
    self = [super init];
    if (self) {
        _offset = offset;
    }
    return self;
}
- (GLKVector4)calcScaleWithElapsedTime:(GLfloat)elapsedTime {
	return GLKVector4Make(0.0f, 0.0f, 0.0f, 1.0f);
}
- (GLKMatrix4)constructMatrixWithElapsedTime:(GLfloat)elapsedTime {
	GLKVector4 theScale = [self calcScaleWithElapsedTime:elapsedTime];
	GLKMatrix4 theMat = GLKMatrix4Identity;
	theMat.m00 = theScale.x;
	theMat.m11 = theScale.y;
	theMat.m22 = theScale.z;
	theMat = GLKMatrix4SetColumn(theMat, 3, self.offset);
	return theMat;
}

- (GLfloat)calcLerpFactorWithElapsedTime:(GLfloat)elapsedTime loopDuration:(GLfloat)loopDuration {
	GLfloat fValue = fmodf(elapsedTime, loopDuration) / loopDuration;
	if (fValue > 0.5f) {
		fValue = 1.0f - fValue;
	}
	return fValue * 2.0f;
}

@end

@implementation Tut06SNullScaleInstance

- (GLKVector4)calcScaleWithElapsedTime:(GLfloat)elapsedTime	{
	return GLKVector4Make(1.0f, 1.0f, 1.0f, 1.0f);
}

@end

@implementation Tut06SStaticUniformScaleInstance

- (GLKVector4)calcScaleWithElapsedTime:(GLfloat)elapsedTime {
	return GLKVector4Make(4.0f, 4.0f, 4.0f, 1.0f);
}

@end

@implementation Tut06SStaticNonUniformScaleInstance

- (GLKVector4)calcScaleWithElapsedTime:(GLfloat)elapsedTime {
	return GLKVector4Make(0.5f, 1.0f, 10.0f, 1.0f);
}

@end

@implementation Tut06SDynamicUniformScaleInstance

- (GLKVector4)calcScaleWithElapsedTime:(GLfloat)elapsedTime {
	GLfloat xLerpFactor = [self calcLerpFactorWithElapsedTime:elapsedTime loopDuration:3.0f];
		//mix(x, y, a) is x + a * (y - x);
	GLfloat	x = 1.0f + xLerpFactor * (4.0f - 1.0f);
	return GLKVector4Make(x, x, x, 1.0f);
}

@end

@implementation Tut06SDynamicNonUniformScaleInstance

- (GLKVector4)calcScaleWithElapsedTime:(GLfloat)elapsedTime {
		//mix(x, y, a) is x + a * (y - x);
	GLfloat xLerpFactor = [self calcLerpFactorWithElapsedTime:elapsedTime loopDuration:3.0f];
	GLfloat zLerpFactor = [self calcLerpFactorWithElapsedTime:elapsedTime loopDuration:5.0f];
	GLfloat x = 1.0f + xLerpFactor * (0.5f - 1.0f);
	GLfloat z = 1.0f + zLerpFactor * (10.0f -1.0f);
	return GLKVector4Make(x, 1.0f, z, 1.0f);
}

@end











